Source: data-priority-map-clojure
Section: java
Priority: optional
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders:
 Apollon Oikonomopoulos <apoikos@debian.org>,
Build-Depends:
 clojure (>= 1.8),
 debhelper-compat (= 13),
 default-jdk,
 javahelper (>= 0.32),
 libbultitude-clojure <!nocheck>,
 maven-repo-helper (>= 1.5~),
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/clojure-team/data-priority-map-clojure.git
Vcs-Browser: https://salsa.debian.org/clojure-team/data-priority-map-clojure
Homepage: https://github.com/clojure/data.priority-map

Package: libdata-priority-map-clojure
Architecture: all
Depends:
 ${java:Depends},
 ${misc:Depends},
Recommends:
 ${java:Recommends},
Multi-Arch: foreign
Description: priority maps for Clojure
 data.priority-map implements priority maps in Clojure. A priority map is very
 similar to a sorted map, however its entries are sorted by value rather than
 by key.
 .
 In addition to supporting all the functions a sorted map supports, a priority
 map can also be thought of as a queue of [item priority] pairs. To support
 usage as a versatile priority queue, priority maps also support the
 conj/peek/pop operations
